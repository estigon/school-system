package com.lampiweb.schoolsystem.repositoryimplement;

import com.lampiweb.schoolsystem.dto.CourseDTO;
import com.lampiweb.schoolsystem.entity.CourseEntity;
import com.lampiweb.schoolsystem.mapper.CourseMapper;
import com.lampiweb.schoolsystem.repository.ICourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class CourseRepositoryImpl {

    @Autowired
    private ICourseRepository iCourseRepository;

    @Autowired
    private CourseMapper courseMapper;

    public List<CourseDTO> getCourses(){
        List<CourseEntity> courseEntityList = (List<CourseEntity>)iCourseRepository.findAll();
        return courseEntityList.stream()
                .map(courseEntity -> courseMapper.courseEntityToCourseDTO(courseEntity))
                .collect(Collectors.toList());
    }

    public Optional<CourseDTO> getCourseById(int id){
        Optional<CourseEntity> courseEntity = iCourseRepository.findById(id);
        return courseEntity.map(course -> courseMapper.courseEntityToCourseDTO(course));
    }

    public CourseDTO saveCurseDTO(CourseDTO courseDTO){
        CourseEntity courseEntity = iCourseRepository
                .save(courseMapper.courseDTOToCourseEntity(courseDTO));
        return courseMapper.courseEntityToCourseDTO(courseEntity);
    }

    public void deleteCourse(int id){
        iCourseRepository.deleteById(id);
    }

}
