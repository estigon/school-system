package com.lampiweb.schoolsystem.repositoryimplement;

import com.lampiweb.schoolsystem.dto.UserDTO;
import com.lampiweb.schoolsystem.entity.UserEntity;
import com.lampiweb.schoolsystem.mapper.UserMapper;
import com.lampiweb.schoolsystem.repository.IUserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserRepositoryImpl {

    @Autowired
    private IUserCrudRepository iUserCrudRepository;

    @Autowired
    private UserMapper userMapper;

    public List<UserDTO> getAllUsers() {
        List<UserEntity> userEntityList = (List<UserEntity>) iUserCrudRepository.findAll();
        List<UserDTO> userDTOList = userEntityList.stream()
                .map(userEntity -> userMapper.userEntityToUserDTO(userEntity)).collect(Collectors.toList());
        return userDTOList;
    }

    public Optional<UserDTO> getUserById(int id){
        Optional<UserEntity> userEntity = iUserCrudRepository.findById(id);
        return userEntity.map(user -> userMapper.userEntityToUserDTO(user));
    }

    public UserDTO saveUser(UserDTO userDTO){
        UserEntity userEntity = iUserCrudRepository.save(userMapper.userDTOtoUserEntity(userDTO));
        return userMapper.userEntityToUserDTO(userEntity);
    }

    public void deleteUser(int id){
        iUserCrudRepository.deleteById(id);
    }
}
