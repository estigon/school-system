package com.lampiweb.schoolsystem.repositoryimplement;

import com.lampiweb.schoolsystem.dto.ProfessorDTO;
import com.lampiweb.schoolsystem.entity.ProfessorEntity;
import com.lampiweb.schoolsystem.mapper.ProfessorMapper;
import com.lampiweb.schoolsystem.repository.IProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ProfessorRepositoryImpl {

    @Autowired
    private IProfessorRepository iProfessorRepository;

    @Autowired
    private ProfessorMapper professorMapper;

    public List<ProfessorDTO> getAllProfessors(){
        List<ProfessorEntity> professorEntityList = (List<ProfessorEntity>)iProfessorRepository.findAll();
        return professorEntityList.stream()
                .map(professorEntity -> professorMapper.professorEntityToProfessorDTO(professorEntity))
                .collect(Collectors.toList());
    }

    public Optional<ProfessorDTO> getProfessorById(int id){
        Optional<ProfessorEntity> professorEntity = iProfessorRepository.findById(id);
        return professorEntity
                .map(professor -> professorMapper.professorEntityToProfessorDTO(professor));
    }

    public void deleteProfessor(int id){
        iProfessorRepository.deleteById(id);
    }

    public ProfessorDTO saveProfessor(ProfessorDTO professorDTO){
        ProfessorEntity professorEntity = iProfessorRepository
                .save(professorMapper.professorDTOToProfessorEntity(professorDTO));
        return professorMapper.professorEntityToProfessorDTO(professorEntity);
    }
}
