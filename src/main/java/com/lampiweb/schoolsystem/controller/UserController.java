package com.lampiweb.schoolsystem.controller;

import com.lampiweb.schoolsystem.dto.UserDTO;
import com.lampiweb.schoolsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/all")
    public ResponseEntity<List<UserDTO>> getAllUsers(){
//  'if guard'
        List<UserDTO> userDTOList;
        try {
            userDTOList = userService.getAllUsers();
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        /*if(userDTOList == null || userDTOList.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else if(userDTOList.size() > 0){
            return new ResponseEntity<>(userDTOList, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }*/
        if(userDTOList == null || userDTOList.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if(!userDTOList.isEmpty()){
            return new ResponseEntity<>(userDTOList, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable("userId") int id){
        return userService.getUserById(id)
                .map( user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElse( new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/save")
    public ResponseEntity<UserDTO> saveUser(@RequestBody UserDTO userDTO){
        UserDTO user =  userService.saveUser(userDTO);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteUser(@PathVariable("id") int id){
        if(userService.deleteUser(id)){
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
