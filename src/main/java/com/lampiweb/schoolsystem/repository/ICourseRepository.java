package com.lampiweb.schoolsystem.repository;

import com.lampiweb.schoolsystem.entity.CourseEntity;
import org.springframework.data.repository.CrudRepository;

public interface ICourseRepository extends CrudRepository<CourseEntity, Integer> {
}
