package com.lampiweb.schoolsystem.repository;

import com.lampiweb.schoolsystem.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface IUserCrudRepository extends CrudRepository<UserEntity, Integer> {
}
