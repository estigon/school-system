package com.lampiweb.schoolsystem.repository;

import com.lampiweb.schoolsystem.entity.ProfessorEntity;
import org.springframework.data.repository.CrudRepository;

public interface IProfessorRepository extends CrudRepository<ProfessorEntity, Integer> {
}
