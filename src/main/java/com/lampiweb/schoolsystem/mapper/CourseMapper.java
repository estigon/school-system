package com.lampiweb.schoolsystem.mapper;

import com.lampiweb.schoolsystem.dto.CourseDTO;
import com.lampiweb.schoolsystem.entity.CourseEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface CourseMapper {

    @Mappings({
            @Mapping(target = "courseName", source = "name")
    })
    CourseDTO courseEntityToCourseDTO(CourseEntity courseEntity);

    @InheritInverseConfiguration
    CourseEntity courseDTOToCourseEntity(CourseDTO courseDTO);
}
