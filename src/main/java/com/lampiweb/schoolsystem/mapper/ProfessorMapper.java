package com.lampiweb.schoolsystem.mapper;

import com.lampiweb.schoolsystem.dto.ProfessorDTO;
import com.lampiweb.schoolsystem.entity.ProfessorEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface ProfessorMapper {

    @Mappings({
            @Mapping(target = "userDTO", source = "userEntity")
    })
    ProfessorDTO professorEntityToProfessorDTO(ProfessorEntity professorEntity);

    @InheritInverseConfiguration
    ProfessorEntity professorDTOToProfessorEntity(ProfessorDTO professorDTO);
}
