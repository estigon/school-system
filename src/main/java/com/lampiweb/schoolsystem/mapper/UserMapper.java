package com.lampiweb.schoolsystem.mapper;
import com.lampiweb.schoolsystem.dto.UserDTO;
import com.lampiweb.schoolsystem.entity.UserEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDTO userEntityToUserDTO(UserEntity userEntity);

    @InheritInverseConfiguration
    UserEntity userDTOtoUserEntity(UserDTO userDTO);
}
