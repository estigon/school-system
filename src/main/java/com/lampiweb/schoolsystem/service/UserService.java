package com.lampiweb.schoolsystem.service;

import com.lampiweb.schoolsystem.dto.UserDTO;
import com.lampiweb.schoolsystem.repositoryimplement.UserRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepositoryImpl userRepository;

    public List<UserDTO> getAllUsers(){
        return userRepository.getAllUsers();
    }

    public Optional<UserDTO> getUserById(int id){
        return userRepository.getUserById(id);
    }

    public UserDTO saveUser(UserDTO userDTO){
        return userRepository.saveUser(userDTO);
    }

    public boolean deleteUser(int id){
        try {
            userRepository.deleteUser(id);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
