package com.lampiweb.schoolsystem.service;

import com.lampiweb.schoolsystem.dto.ProfessorDTO;
import com.lampiweb.schoolsystem.repositoryimplement.ProfessorRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProfessorService {

    @Autowired
    private ProfessorRepositoryImpl professorRepository;

    public ResponseEntity<List<ProfessorDTO>> getProfessors(){
        List<ProfessorDTO> professorDTOList = professorRepository.getAllProfessors();
        if(professorDTOList == null || professorDTOList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(professorDTOList, HttpStatus.OK);
    }

    public ResponseEntity<ProfessorDTO> getProfessorById(int id){
        Optional<ProfessorDTO> professorDTO = professorRepository.getProfessorById(id);
        return professorDTO.map(professor ->  new ResponseEntity<>(professor, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    public ResponseEntity<ProfessorDTO> saveProfessor(ProfessorDTO professorDTO){
        ProfessorDTO professor = professorRepository.saveProfessor(professorDTO);
        if (professor == null){//que codigo debe mostrarse al fallar una creacion?
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(professor, HttpStatus.CREATED);
    }

    public ResponseEntity deleteProfessor(int id){
        try {
            professorRepository.deleteProfessor(id);
            return new ResponseEntity(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
