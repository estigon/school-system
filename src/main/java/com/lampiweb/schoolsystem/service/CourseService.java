package com.lampiweb.schoolsystem.service;

import com.lampiweb.schoolsystem.dto.CourseDTO;
import com.lampiweb.schoolsystem.repositoryimplement.CourseRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseService {

    @Autowired
    private CourseRepositoryImpl courseRepository;

    public ResponseEntity<List<CourseDTO>> getCourses(){
        List<CourseDTO> courseDTOList = courseRepository.getCourses();
        if(courseDTOList == null || courseDTOList.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(courseDTOList, HttpStatus.OK);
    }

    public ResponseEntity<CourseDTO> getCourseById(int id){
        Optional<CourseDTO> courseDTO = courseRepository.getCourseById(id);
        return courseDTO.map(course -> new ResponseEntity<>(course, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    public ResponseEntity<CourseDTO> saveCourse(CourseDTO courseDTO){
        CourseDTO course = courseRepository.saveCurseDTO(courseDTO);
        if (course == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(course, HttpStatus.CREATED);
    }

    public ResponseEntity deleteCourse(int id){
        try{
            courseRepository.deleteCourse(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch(Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
